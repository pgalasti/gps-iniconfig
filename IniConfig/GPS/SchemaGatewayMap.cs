﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IniConfig.GPS
{
    static class SchemaGatewayMap
    {
        private static Dictionary<String, String> gatewayToSchemaDictionary;
        private static Dictionary<String, String> schemaToGatewayDictionary;

     
        static SchemaGatewayMap()
        {
            gatewayToSchemaDictionary = new Dictionary<String, String>();
            gatewayToSchemaDictionary.Add("US_PLMDI_PROD_01", "SMPAWP1");
            gatewayToSchemaDictionary.Add("US_PLMDI_TRAIN_01", "SMPAWP7");
            gatewayToSchemaDictionary.Add("US_PLMDI_TMDL_01", "SMPAWP3");
            gatewayToSchemaDictionary.Add("US_PLMDI_TEST_01", "SMPAWP2");
            gatewayToSchemaDictionary.Add("US_PLMDI_PTST_01", "SMPAWP6");

            schemaToGatewayDictionary = new Dictionary<String, String>();
            schemaToGatewayDictionary.Add("SMPAWP1", "US_PLMDI_PROD_01");
            schemaToGatewayDictionary.Add("SMPAWP7", "US_PLMDI_TRAIN_01");
            schemaToGatewayDictionary.Add("SMPAWP3", "US_PLMDI_TMDL_01");
            schemaToGatewayDictionary.Add("SMPAWP2", "US_PLMDI_TEST_01");
            schemaToGatewayDictionary.Add("SMPAWP6", "US_PLMDI_PTST_01");
        }

        public static String LookupSchema(String gateway)
        {
            String returnedSchema;
            gatewayToSchemaDictionary.TryGetValue(gateway, out returnedSchema);

            if (String.IsNullOrEmpty(returnedSchema))
                returnedSchema = String.Empty;

            return returnedSchema;
        }

        public static String LookupGateway(String schema)
        {
            String returnedGateway;
            schemaToGatewayDictionary.TryGetValue(schema, out returnedGateway);

            if (String.IsNullOrEmpty(returnedGateway))
                returnedGateway = String.Empty;

            return returnedGateway;

        }
    }
}
