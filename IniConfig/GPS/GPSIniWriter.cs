﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IniConfig.IO;
using IniConfig.Structure;

namespace IniConfig.GPS
{
    class GPSIniWriter
    {
        protected IIniWriter writer;

        public GPSIniWriter(IIniWriter writer)
        {
            this.writer = writer;
        }

        public void WriteElements(GpsElements elements)
        {
            
            writer.WriteSectionItem(GpsElements.GPS_SECTION);

            writer.WriteLineItem("ExcelPath", elements.ExcelPath);
            writer.WriteLineItem("ReaderPath", elements.ReaderPath);
            writer.WriteLineItem("ReaderSwitches", elements.ReaderSwitches);
            writer.WriteLineItem("ReaderOverride", elements.ReaderOverride);

            writer.WriteBlankLine(); // Separate Gateways

            writer.WriteLineItem("Gateway", elements.Gateway);

            writer.WriteBlankLine(); // Separate GPS User

            writer.WriteLineItem("GPSUser", elements.GPSUser);
            writer.WriteLineItem("CurrentGPSVersion", elements.CurrentGPSVersion);
            writer.WriteLineItem("AllVersions", elements.AllVersions);

            writer.WriteBlankLine(); // Separate Paths

            writer.WriteLineItem("CrystalReportsPath", elements.CrystalReportsPath);
            writer.WriteLineItem("GPSSourceFilesPath", elements.GPSSourceFilesPath);
            writer.WriteLineItem("GPSWorkFilesPath", elements.GPSWorkFilesPath);
            writer.WriteLineItem("GPSUserPreferencePath", elements.GPSUserPreferencePath);

            // Some static hard coded values
            writer.WriteLineItem("LastPCSetupDate", "02-Aug-10");
            writer.WriteLineItem("PasswordServer", "US_PSWSRV_01");
            writer.WriteLineItem("UserDir", "Y");

            writer.WriteBlankLine(); // Separate Mode

            if (elements.Mode.Trim() != String.Empty)
            {
                writer.WriteComment("Optional for debug");
                writer.WriteLineItem("Mode", elements.Mode);
            }

            writer.WriteBlankLine();

            // ConfigFiles Section
            writer.WriteSectionItem(GpsElements.CONFIG_FILES_SECTION);

            writer.WriteLineItem("NumberOfTags", elements.NumberOfTags);
            writer.WriteLineItem("FileTag1", elements.FileTag1);
            writer.WriteLineItem("FileTag2", elements.FileTag2);

            // Could be optional
            if (elements.FileTag3.Trim() != String.Empty)
                writer.WriteLineItem("FileTag3", elements.FileTag3);

            writer.WriteLineItem("TEMPLATE_TREE", elements.TEMPLATE_TREE);
            writer.WriteLineItem("DD_CONFIG", elements.DD_CONFIG);

            // Could be optional
            if (elements.TEMPLATE_TREE_V18.Trim() != String.Empty)
                writer.WriteLineItem("TEMPLATE_TREE_V18", elements.TEMPLATE_TREE_V18);

            writer.WriteBlankLine();

            // Defaults Section
            writer.WriteSectionItem("Defaults");

            writer.WriteLineItem("SerializeWaterMark", elements.SerializeWaterMark);
            writer.WriteLineItem("EnableSaveReminder", elements.EnableSaveReminder);
            writer.WriteLineItem("SaveReminderInterval", elements.SaveReminderInterval);
            writer.WriteLineItem("NumberOfArchivedNodes", elements.NumberOfArchivedNodes);

            writer.WriteBlankLine();

            // GPS Components Section
            writer.WriteSectionItem(GpsElements.GPS_COMPONENTS_SECTION);


            const String component = "Component";
            int i = 1;
            foreach(String item in elements.Components)
            {
                String componentWithNumber = component + i++;
                writer.WriteLineItem(componentWithNumber, item);
            }

            //writer.WriteLineItem("NbrOfComponents", elements.NbrOfComponents);
            writer.WriteLineItem("NbrOfComponents", (i-1).ToString());

            writer.WriteBlankLine();

            // Binaries section
            writer.WriteSectionItem(elements.CurrentGPSVersion);
            writer.WriteLineItem("GPSPath", elements.GPSPath);

            this.writer.CloseWriter();
        }


        public override String ToString()
        {
            return this.writer.ToString();
        }
        
    }
}
