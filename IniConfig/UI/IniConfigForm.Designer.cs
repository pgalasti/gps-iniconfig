﻿namespace IniConfig
{
    partial class IniConfig
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.cbxLoadProfile = new System.Windows.Forms.ComboBox();
            this.txtProfileName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnLoadIni = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtSchema = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.txtGpsContIniPath = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.txtVersion = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtBinaryDirectory = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label28 = new System.Windows.Forms.Label();
            this.txtMode = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtGpsUser = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtGpsSourceFilesPath = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtCrystalReportsPath = new System.Windows.Forms.TextBox();
            this.ckbxReaderOverride = new System.Windows.Forms.CheckBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtGpsUserPreferencePath = new System.Windows.Forms.TextBox();
            this.txtGpsWorkFilesPath = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtSourceFileFolderName = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtCrystalFolderReportName = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtReaderSwitches = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtReaderPath = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtExcelPath = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cbxGateway = new System.Windows.Forms.ComboBox();
            this.rtxIniText = new System.Windows.Forms.RichTextBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.label22 = new System.Windows.Forms.Label();
            this.txtNumberOfTags = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtTemplateTreeV18 = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtDDConfig = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtTemplateTree = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtFileTag3 = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtFileTag2 = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtFileTag1 = new System.Windows.Forms.TextBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.ckbxEnableSaveReminder = new System.Windows.Forms.CheckBox();
            this.label26 = new System.Windows.Forms.Label();
            this.txtNumberOfArchivedNodes = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.txtSaveReminderInterval = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.txtSerializeWaterMark = new System.Windows.Forms.TextBox();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.btnRemoveComponent = new System.Windows.Forms.Button();
            this.btnAddComponent = new System.Windows.Forms.Button();
            this.txtAddComponent = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.txtNbrOfComponents = new System.Windows.Forms.TextBox();
            this.lstComponents = new System.Windows.Forms.ListBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnApply = new System.Windows.Forms.Button();
            this.btnDeleteProfile = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Load Profile";
            // 
            // cbxLoadProfile
            // 
            this.cbxLoadProfile.FormattingEnabled = true;
            this.cbxLoadProfile.Location = new System.Drawing.Point(12, 24);
            this.cbxLoadProfile.Name = "cbxLoadProfile";
            this.cbxLoadProfile.Size = new System.Drawing.Size(198, 21);
            this.cbxLoadProfile.TabIndex = 0;
            this.cbxLoadProfile.SelectedIndexChanged += new System.EventHandler(this.Profile_Update);
            // 
            // txtProfileName
            // 
            this.txtProfileName.Location = new System.Drawing.Point(255, 24);
            this.txtProfileName.Name = "txtProfileName";
            this.txtProfileName.Size = new System.Drawing.Size(158, 20);
            this.txtProfileName.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(255, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(36, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Profile";
            // 
            // btnLoadIni
            // 
            this.btnLoadIni.Location = new System.Drawing.Point(609, 22);
            this.btnLoadIni.Name = "btnLoadIni";
            this.btnLoadIni.Size = new System.Drawing.Size(118, 23);
            this.btnLoadIni.TabIndex = 4;
            this.btnLoadIni.Text = "Load GPS.INI...";
            this.btnLoadIni.UseVisualStyleBackColor = true;
            this.btnLoadIni.Click += new System.EventHandler(this.LoadIniFile_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtSchema);
            this.groupBox1.Controls.Add(this.label30);
            this.groupBox1.Controls.Add(this.label29);
            this.groupBox1.Controls.Add(this.txtGpsContIniPath);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.txtVersion);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtBinaryDirectory);
            this.groupBox1.Location = new System.Drawing.Point(12, 76);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(591, 102);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "General";
            // 
            // txtSchema
            // 
            this.txtSchema.Location = new System.Drawing.Point(451, 76);
            this.txtSchema.Name = "txtSchema";
            this.txtSchema.ReadOnly = true;
            this.txtSchema.Size = new System.Drawing.Size(134, 20);
            this.txtSchema.TabIndex = 17;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(448, 60);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(46, 13);
            this.label30.TabIndex = 16;
            this.label30.Text = "Schema";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(142, 60);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(101, 13);
            this.label29.TabIndex = 15;
            this.label29.Text = "GPSCONT.INI Path";
            // 
            // txtGpsContIniPath
            // 
            this.txtGpsContIniPath.Location = new System.Drawing.Point(145, 76);
            this.txtGpsContIniPath.Name = "txtGpsContIniPath";
            this.txtGpsContIniPath.ReadOnly = true;
            this.txtGpsContIniPath.Size = new System.Drawing.Size(291, 20);
            this.txtGpsContIniPath.TabIndex = 14;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(6, 63);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(112, 23);
            this.button1.TabIndex = 13;
            this.button1.Text = "Browse...";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.BrowseForBinaryDirectory_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(362, 16);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(42, 13);
            this.label12.TabIndex = 4;
            this.label12.Text = "Version";
            // 
            // txtVersion
            // 
            this.txtVersion.Location = new System.Drawing.Point(365, 32);
            this.txtVersion.Name = "txtVersion";
            this.txtVersion.Size = new System.Drawing.Size(80, 20);
            this.txtVersion.TabIndex = 3;
            this.txtVersion.TextChanged += new System.EventHandler(this.Field_Update);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Binary Directory";
            // 
            // txtBinaryDirectory
            // 
            this.txtBinaryDirectory.Location = new System.Drawing.Point(6, 32);
            this.txtBinaryDirectory.Name = "txtBinaryDirectory";
            this.txtBinaryDirectory.ReadOnly = true;
            this.txtBinaryDirectory.Size = new System.Drawing.Size(353, 20);
            this.txtBinaryDirectory.TabIndex = 0;
            this.txtBinaryDirectory.TextChanged += new System.EventHandler(this.BinaryDirectory_Update);
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox2.Controls.Add(this.label28);
            this.groupBox2.Controls.Add(this.txtMode);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.txtGpsUser);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.txtGpsSourceFilesPath);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.txtCrystalReportsPath);
            this.groupBox2.Controls.Add(this.ckbxReaderOverride);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.txtGpsUserPreferencePath);
            this.groupBox2.Controls.Add(this.txtGpsWorkFilesPath);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.txtSourceFileFolderName);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.txtCrystalFolderReportName);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.txtReaderSwitches);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.txtReaderPath);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.txtExcelPath);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.cbxGateway);
            this.groupBox2.Location = new System.Drawing.Point(6, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(694, 176);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(461, 108);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(82, 13);
            this.label28.TabIndex = 25;
            this.label28.Text = "Mode (Optional)";
            // 
            // txtMode
            // 
            this.txtMode.Location = new System.Drawing.Point(549, 105);
            this.txtMode.Name = "txtMode";
            this.txtMode.Size = new System.Drawing.Size(80, 20);
            this.txtMode.TabIndex = 24;
            this.txtMode.TextChanged += new System.EventHandler(this.Field_Update);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(6, 134);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(51, 13);
            this.label15.TabIndex = 23;
            this.label15.Text = "GPSUser";
            // 
            // txtGpsUser
            // 
            this.txtGpsUser.Location = new System.Drawing.Point(9, 150);
            this.txtGpsUser.Name = "txtGpsUser";
            this.txtGpsUser.ReadOnly = true;
            this.txtGpsUser.Size = new System.Drawing.Size(211, 20);
            this.txtGpsUser.TabIndex = 22;
            this.txtGpsUser.TextChanged += new System.EventHandler(this.Field_Update);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(223, 134);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(106, 13);
            this.label14.TabIndex = 21;
            this.label14.Text = "GPSSourceFilesPath";
            // 
            // txtGpsSourceFilesPath
            // 
            this.txtGpsSourceFilesPath.Location = new System.Drawing.Point(226, 150);
            this.txtGpsSourceFilesPath.Name = "txtGpsSourceFilesPath";
            this.txtGpsSourceFilesPath.ReadOnly = true;
            this.txtGpsSourceFilesPath.Size = new System.Drawing.Size(207, 20);
            this.txtGpsSourceFilesPath.TabIndex = 20;
            this.txtGpsSourceFilesPath.TextChanged += new System.EventHandler(this.Field_Update);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(436, 134);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(97, 13);
            this.label13.TabIndex = 19;
            this.label13.Text = "CrystalReportsPath";
            // 
            // txtCrystalReportsPath
            // 
            this.txtCrystalReportsPath.Location = new System.Drawing.Point(439, 150);
            this.txtCrystalReportsPath.Name = "txtCrystalReportsPath";
            this.txtCrystalReportsPath.ReadOnly = true;
            this.txtCrystalReportsPath.Size = new System.Drawing.Size(236, 20);
            this.txtCrystalReportsPath.TabIndex = 18;
            this.txtCrystalReportsPath.TextChanged += new System.EventHandler(this.Field_Update);
            // 
            // ckbxReaderOverride
            // 
            this.ckbxReaderOverride.AutoSize = true;
            this.ckbxReaderOverride.Location = new System.Drawing.Point(9, 105);
            this.ckbxReaderOverride.Name = "ckbxReaderOverride";
            this.ckbxReaderOverride.Size = new System.Drawing.Size(101, 17);
            this.ckbxReaderOverride.TabIndex = 17;
            this.ckbxReaderOverride.Text = "ReaderOverride";
            this.ckbxReaderOverride.UseVisualStyleBackColor = true;
            this.ckbxReaderOverride.CheckedChanged += new System.EventHandler(this.Field_Update);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 60);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(125, 13);
            this.label11.TabIndex = 16;
            this.label11.Text = "GPSUserPreferencePath";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(326, 60);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(98, 13);
            this.label10.TabIndex = 14;
            this.label10.Text = "GPSWorkFilesPath";
            // 
            // txtGpsUserPreferencePath
            // 
            this.txtGpsUserPreferencePath.Location = new System.Drawing.Point(9, 76);
            this.txtGpsUserPreferencePath.Name = "txtGpsUserPreferencePath";
            this.txtGpsUserPreferencePath.Size = new System.Drawing.Size(158, 20);
            this.txtGpsUserPreferencePath.TabIndex = 15;
            this.txtGpsUserPreferencePath.TextChanged += new System.EventHandler(this.Field_Update);
            // 
            // txtGpsWorkFilesPath
            // 
            this.txtGpsWorkFilesPath.Location = new System.Drawing.Point(329, 76);
            this.txtGpsWorkFilesPath.Name = "txtGpsWorkFilesPath";
            this.txtGpsWorkFilesPath.Size = new System.Drawing.Size(158, 20);
            this.txtGpsWorkFilesPath.TabIndex = 13;
            this.txtGpsWorkFilesPath.TextChanged += new System.EventHandler(this.Field_Update);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(493, 60);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(123, 13);
            this.label9.TabIndex = 12;
            this.label9.Text = "Source File Folder Name";
            // 
            // txtSourceFileFolderName
            // 
            this.txtSourceFileFolderName.Location = new System.Drawing.Point(496, 76);
            this.txtSourceFileFolderName.Name = "txtSourceFileFolderName";
            this.txtSourceFileFolderName.Size = new System.Drawing.Size(133, 20);
            this.txtSourceFileFolderName.TabIndex = 11;
            this.txtSourceFileFolderName.TextChanged += new System.EventHandler(this.SourceFileFolder_Update);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(493, 21);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(136, 13);
            this.label8.TabIndex = 10;
            this.label8.Text = "Crystal Folder Report Name";
            // 
            // txtCrystalFolderReportName
            // 
            this.txtCrystalFolderReportName.Location = new System.Drawing.Point(496, 37);
            this.txtCrystalFolderReportName.Name = "txtCrystalFolderReportName";
            this.txtCrystalFolderReportName.Size = new System.Drawing.Size(133, 20);
            this.txtCrystalFolderReportName.TabIndex = 9;
            this.txtCrystalFolderReportName.TextChanged += new System.EventHandler(this.CrystalFolderReportName_Update);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(165, 60);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(85, 13);
            this.label7.TabIndex = 8;
            this.label7.Text = "ReaderSwitches";
            // 
            // txtReaderSwitches
            // 
            this.txtReaderSwitches.Location = new System.Drawing.Point(168, 76);
            this.txtReaderSwitches.Name = "txtReaderSwitches";
            this.txtReaderSwitches.Size = new System.Drawing.Size(158, 20);
            this.txtReaderSwitches.TabIndex = 7;
            this.txtReaderSwitches.TextChanged += new System.EventHandler(this.Field_Update);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(329, 21);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "ReaderPath";
            // 
            // txtReaderPath
            // 
            this.txtReaderPath.Location = new System.Drawing.Point(332, 37);
            this.txtReaderPath.Name = "txtReaderPath";
            this.txtReaderPath.Size = new System.Drawing.Size(158, 20);
            this.txtReaderPath.TabIndex = 5;
            this.txtReaderPath.TextChanged += new System.EventHandler(this.Field_Update);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(165, 21);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(55, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "ExcelPath";
            // 
            // txtExcelPath
            // 
            this.txtExcelPath.Location = new System.Drawing.Point(168, 37);
            this.txtExcelPath.Name = "txtExcelPath";
            this.txtExcelPath.Size = new System.Drawing.Size(158, 20);
            this.txtExcelPath.TabIndex = 3;
            this.txtExcelPath.TextChanged += new System.EventHandler(this.Field_Update);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 21);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Gateway";
            // 
            // cbxGateway
            // 
            this.cbxGateway.FormattingEnabled = true;
            this.cbxGateway.Items.AddRange(new object[] {
            "US_PLMDI_PROD_01",
            "US_PLMDI_TRAIN_01",
            "US_PLMDI_TMDL_01",
            "US_PLMDI_MODL_01",
            "US_PLMDI_PTST_01",
            "US_PLMDI_TEST_01",
            "US_PLMDI_LDA_01",
            "US_PLMDI_LDA_02"});
            this.cbxGateway.Location = new System.Drawing.Point(9, 37);
            this.cbxGateway.Name = "cbxGateway";
            this.cbxGateway.Size = new System.Drawing.Size(153, 21);
            this.cbxGateway.TabIndex = 0;
            this.cbxGateway.TextChanged += new System.EventHandler(this.GatewayHasChanged_Update);
            // 
            // rtxIniText
            // 
            this.rtxIniText.BackColor = System.Drawing.Color.White;
            this.rtxIniText.Location = new System.Drawing.Point(14, 404);
            this.rtxIniText.Name = "rtxIniText";
            this.rtxIniText.ReadOnly = true;
            this.rtxIniText.Size = new System.Drawing.Size(707, 327);
            this.rtxIniText.TabIndex = 7;
            this.rtxIniText.Text = "";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Location = new System.Drawing.Point(14, 184);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(711, 218);
            this.tabControl1.TabIndex = 9;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(703, 192);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "GPS";
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage2.Controls.Add(this.label22);
            this.tabPage2.Controls.Add(this.txtNumberOfTags);
            this.tabPage2.Controls.Add(this.label21);
            this.tabPage2.Controls.Add(this.txtTemplateTreeV18);
            this.tabPage2.Controls.Add(this.label20);
            this.tabPage2.Controls.Add(this.txtDDConfig);
            this.tabPage2.Controls.Add(this.label19);
            this.tabPage2.Controls.Add(this.txtTemplateTree);
            this.tabPage2.Controls.Add(this.label18);
            this.tabPage2.Controls.Add(this.txtFileTag3);
            this.tabPage2.Controls.Add(this.label17);
            this.tabPage2.Controls.Add(this.txtFileTag2);
            this.tabPage2.Controls.Add(this.label16);
            this.tabPage2.Controls.Add(this.txtFileTag1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(703, 192);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "ConfigFiles";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(536, 6);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(79, 13);
            this.label22.TabIndex = 31;
            this.label22.Text = "NumberOfTags";
            // 
            // txtNumberOfTags
            // 
            this.txtNumberOfTags.Location = new System.Drawing.Point(539, 22);
            this.txtNumberOfTags.Name = "txtNumberOfTags";
            this.txtNumberOfTags.ReadOnly = true;
            this.txtNumberOfTags.Size = new System.Drawing.Size(76, 20);
            this.txtNumberOfTags.TabIndex = 30;
            this.txtNumberOfTags.TextChanged += new System.EventHandler(this.Field_Update);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(252, 114);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(124, 13);
            this.label21.TabIndex = 29;
            this.label21.Text = "TEMPLATE_TREE_V18";
            // 
            // txtTemplateTreeV18
            // 
            this.txtTemplateTreeV18.Location = new System.Drawing.Point(255, 130);
            this.txtTemplateTreeV18.Name = "txtTemplateTreeV18";
            this.txtTemplateTreeV18.ReadOnly = true;
            this.txtTemplateTreeV18.Size = new System.Drawing.Size(258, 20);
            this.txtTemplateTreeV18.TabIndex = 28;
            this.txtTemplateTreeV18.TextChanged += new System.EventHandler(this.Field_Update);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(252, 60);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(69, 13);
            this.label20.TabIndex = 27;
            this.label20.Text = "DD_CONFIG";
            // 
            // txtDDConfig
            // 
            this.txtDDConfig.Location = new System.Drawing.Point(255, 76);
            this.txtDDConfig.Name = "txtDDConfig";
            this.txtDDConfig.ReadOnly = true;
            this.txtDDConfig.Size = new System.Drawing.Size(258, 20);
            this.txtDDConfig.TabIndex = 26;
            this.txtDDConfig.TextChanged += new System.EventHandler(this.Field_Update);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(252, 6);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(99, 13);
            this.label19.TabIndex = 25;
            this.label19.Text = "TEMPLATE_TREE";
            // 
            // txtTemplateTree
            // 
            this.txtTemplateTree.Location = new System.Drawing.Point(255, 22);
            this.txtTemplateTree.Name = "txtTemplateTree";
            this.txtTemplateTree.ReadOnly = true;
            this.txtTemplateTree.Size = new System.Drawing.Size(258, 20);
            this.txtTemplateTree.TabIndex = 24;
            this.txtTemplateTree.TextChanged += new System.EventHandler(this.Field_Update);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(3, 114);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(48, 13);
            this.label18.TabIndex = 10;
            this.label18.Text = "FileTag3";
            // 
            // txtFileTag3
            // 
            this.txtFileTag3.Location = new System.Drawing.Point(6, 130);
            this.txtFileTag3.Name = "txtFileTag3";
            this.txtFileTag3.Size = new System.Drawing.Size(158, 20);
            this.txtFileTag3.TabIndex = 9;
            this.txtFileTag3.TextChanged += new System.EventHandler(this.FileTag3_Update);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(3, 60);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(48, 13);
            this.label17.TabIndex = 8;
            this.label17.Text = "FileTag2";
            // 
            // txtFileTag2
            // 
            this.txtFileTag2.Location = new System.Drawing.Point(6, 76);
            this.txtFileTag2.Name = "txtFileTag2";
            this.txtFileTag2.Size = new System.Drawing.Size(158, 20);
            this.txtFileTag2.TabIndex = 7;
            this.txtFileTag2.TextChanged += new System.EventHandler(this.FileTag2_Update);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(3, 6);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(48, 13);
            this.label16.TabIndex = 6;
            this.label16.Text = "FileTag1";
            // 
            // txtFileTag1
            // 
            this.txtFileTag1.Location = new System.Drawing.Point(6, 22);
            this.txtFileTag1.Name = "txtFileTag1";
            this.txtFileTag1.Size = new System.Drawing.Size(158, 20);
            this.txtFileTag1.TabIndex = 5;
            this.txtFileTag1.TextChanged += new System.EventHandler(this.FileTag1_Update);
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage3.Controls.Add(this.ckbxEnableSaveReminder);
            this.tabPage3.Controls.Add(this.label26);
            this.tabPage3.Controls.Add(this.txtNumberOfArchivedNodes);
            this.tabPage3.Controls.Add(this.label25);
            this.tabPage3.Controls.Add(this.txtSaveReminderInterval);
            this.tabPage3.Controls.Add(this.label24);
            this.tabPage3.Controls.Add(this.label23);
            this.tabPage3.Controls.Add(this.txtSerializeWaterMark);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(703, 192);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Defaults";
            // 
            // ckbxEnableSaveReminder
            // 
            this.ckbxEnableSaveReminder.AutoSize = true;
            this.ckbxEnableSaveReminder.Location = new System.Drawing.Point(15, 67);
            this.ckbxEnableSaveReminder.Name = "ckbxEnableSaveReminder";
            this.ckbxEnableSaveReminder.Size = new System.Drawing.Size(44, 17);
            this.ckbxEnableSaveReminder.TabIndex = 15;
            this.ckbxEnableSaveReminder.Text = "Yes";
            this.ckbxEnableSaveReminder.UseVisualStyleBackColor = true;
            this.ckbxEnableSaveReminder.CheckedChanged += new System.EventHandler(this.Field_Update);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(340, 12);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(131, 13);
            this.label26.TabIndex = 14;
            this.label26.Text = "NumberOfArchivedNodes ";
            // 
            // txtNumberOfArchivedNodes
            // 
            this.txtNumberOfArchivedNodes.Location = new System.Drawing.Point(343, 28);
            this.txtNumberOfArchivedNodes.Name = "txtNumberOfArchivedNodes";
            this.txtNumberOfArchivedNodes.Size = new System.Drawing.Size(158, 20);
            this.txtNumberOfArchivedNodes.TabIndex = 13;
            this.txtNumberOfArchivedNodes.TextChanged += new System.EventHandler(this.Field_Update);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(175, 12);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(115, 13);
            this.label25.TabIndex = 12;
            this.label25.Text = "SaveReminderInterval ";
            // 
            // txtSaveReminderInterval
            // 
            this.txtSaveReminderInterval.Location = new System.Drawing.Point(178, 28);
            this.txtSaveReminderInterval.Name = "txtSaveReminderInterval";
            this.txtSaveReminderInterval.Size = new System.Drawing.Size(158, 20);
            this.txtSaveReminderInterval.TabIndex = 11;
            this.txtSaveReminderInterval.TextChanged += new System.EventHandler(this.Field_Update);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(12, 51);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(113, 13);
            this.label24.TabIndex = 10;
            this.label24.Text = "EnableSaveReminder ";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(12, 12);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(102, 13);
            this.label23.TabIndex = 8;
            this.label23.Text = "SerializeWaterMark ";
            // 
            // txtSerializeWaterMark
            // 
            this.txtSerializeWaterMark.Location = new System.Drawing.Point(15, 28);
            this.txtSerializeWaterMark.Name = "txtSerializeWaterMark";
            this.txtSerializeWaterMark.Size = new System.Drawing.Size(158, 20);
            this.txtSerializeWaterMark.TabIndex = 7;
            this.txtSerializeWaterMark.TextChanged += new System.EventHandler(this.Field_Update);
            // 
            // tabPage4
            // 
            this.tabPage4.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage4.Controls.Add(this.btnRemoveComponent);
            this.tabPage4.Controls.Add(this.btnAddComponent);
            this.tabPage4.Controls.Add(this.txtAddComponent);
            this.tabPage4.Controls.Add(this.label27);
            this.tabPage4.Controls.Add(this.txtNbrOfComponents);
            this.tabPage4.Controls.Add(this.lstComponents);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(703, 192);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "GPS Components";
            // 
            // btnRemoveComponent
            // 
            this.btnRemoveComponent.Location = new System.Drawing.Point(214, 13);
            this.btnRemoveComponent.Name = "btnRemoveComponent";
            this.btnRemoveComponent.Size = new System.Drawing.Size(70, 23);
            this.btnRemoveComponent.TabIndex = 30;
            this.btnRemoveComponent.Text = "Remove";
            this.btnRemoveComponent.UseVisualStyleBackColor = true;
            this.btnRemoveComponent.Click += new System.EventHandler(this.RemoveComponentFromList_Click);
            // 
            // btnAddComponent
            // 
            this.btnAddComponent.Location = new System.Drawing.Point(363, 151);
            this.btnAddComponent.Name = "btnAddComponent";
            this.btnAddComponent.Size = new System.Drawing.Size(50, 23);
            this.btnAddComponent.TabIndex = 29;
            this.btnAddComponent.Text = "Add";
            this.btnAddComponent.UseVisualStyleBackColor = true;
            this.btnAddComponent.Click += new System.EventHandler(this.AddComponentToList_Click);
            // 
            // txtAddComponent
            // 
            this.txtAddComponent.Location = new System.Drawing.Point(214, 153);
            this.txtAddComponent.Name = "txtAddComponent";
            this.txtAddComponent.Size = new System.Drawing.Size(143, 20);
            this.txtAddComponent.TabIndex = 28;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(528, 14);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(97, 13);
            this.label27.TabIndex = 27;
            this.label27.Text = "NbrOfComponents ";
            // 
            // txtNbrOfComponents
            // 
            this.txtNbrOfComponents.Location = new System.Drawing.Point(531, 30);
            this.txtNbrOfComponents.Name = "txtNbrOfComponents";
            this.txtNbrOfComponents.ReadOnly = true;
            this.txtNbrOfComponents.Size = new System.Drawing.Size(158, 20);
            this.txtNbrOfComponents.TabIndex = 26;
            this.txtNbrOfComponents.TextChanged += new System.EventHandler(this.Field_Update);
            // 
            // lstComponents
            // 
            this.lstComponents.FormattingEnabled = true;
            this.lstComponents.Location = new System.Drawing.Point(5, 13);
            this.lstComponents.Name = "lstComponents";
            this.lstComponents.ScrollAlwaysVisible = true;
            this.lstComponents.Size = new System.Drawing.Size(203, 160);
            this.lstComponents.TabIndex = 1;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(419, 24);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(112, 23);
            this.btnSave.TabIndex = 10;
            this.btnSave.Text = "Save Profile";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.SaveProfile_Click);
            // 
            // btnApply
            // 
            this.btnApply.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.btnApply.Location = new System.Drawing.Point(609, 172);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(112, 28);
            this.btnApply.TabIndex = 11;
            this.btnApply.Text = "Launch GPS";
            this.btnApply.UseVisualStyleBackColor = false;
            this.btnApply.Click += new System.EventHandler(this.ApplyToIniFiles_Click);
            // 
            // btnDeleteProfile
            // 
            this.btnDeleteProfile.Location = new System.Drawing.Point(419, 53);
            this.btnDeleteProfile.Name = "btnDeleteProfile";
            this.btnDeleteProfile.Size = new System.Drawing.Size(112, 23);
            this.btnDeleteProfile.TabIndex = 12;
            this.btnDeleteProfile.Text = "Delete Profile";
            this.btnDeleteProfile.UseVisualStyleBackColor = true;
            this.btnDeleteProfile.Click += new System.EventHandler(this.DeleteProfile_Click);
            // 
            // IniConfig
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(739, 739);
            this.Controls.Add(this.btnDeleteProfile);
            this.Controls.Add(this.btnApply);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.rtxIniText);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnLoadIni);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtProfileName);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbxLoadProfile);
            this.MaximizeBox = false;
            this.Name = "IniConfig";
            this.Text = "GPS Configuration Profile Manager";
            this.Load += new System.EventHandler(this.OnForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbxLoadProfile;
        private System.Windows.Forms.TextBox txtProfileName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnLoadIni;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtBinaryDirectory;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cbxGateway;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtVersion;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtGpsUserPreferencePath;
        private System.Windows.Forms.TextBox txtGpsWorkFilesPath;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtSourceFileFolderName;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtCrystalFolderReportName;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtReaderSwitches;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtReaderPath;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtExcelPath;
        private System.Windows.Forms.RichTextBox rtxIniText;
        private System.Windows.Forms.CheckBox ckbxReaderOverride;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtGpsUser;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtGpsSourceFilesPath;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtCrystalReportsPath;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txtNumberOfTags;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtTemplateTreeV18;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtDDConfig;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtTemplateTree;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtFileTag3;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtFileTag2;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtFileTag1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.CheckBox ckbxEnableSaveReminder;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox txtNumberOfArchivedNodes;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox txtSaveReminderInterval;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txtSerializeWaterMark;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox txtNbrOfComponents;
        private System.Windows.Forms.ListBox lstComponents;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnRemoveComponent;
        private System.Windows.Forms.Button btnAddComponent;
        private System.Windows.Forms.TextBox txtAddComponent;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox txtMode;
        private System.Windows.Forms.Button btnApply;
        private System.Windows.Forms.Button btnDeleteProfile;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox txtGpsContIniPath;
        private System.Windows.Forms.TextBox txtSchema;
        private System.Windows.Forms.Label label30;
    }
}

