﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using IniConfig.Structure;
using IniConfig.IO;
using IniConfig.GPS;
using System.Diagnostics;

namespace IniConfig
{
    public partial class IniConfig : Form
    {
        // Boolean to check if form is in middle of updating
        private Boolean inProcessOfUpdating;

        public IniConfig()
        {
            InitializeComponent();
        }

        // Loads a GPS.INI file to the form and preview box
        private void LoadIniFile(String filePath)
        {
            IniFile iniFile = new IniFile(filePath);
            
            this.LoadFormWithIniFile(iniFile);

            this.ReadFileToPreviewBox(filePath);
        }

        // Loads form fields with the GPS.INI file
        private void LoadFormWithIniFile(IniFile iniFile)
        {
            String binaryPathToUse = String.Empty;
            String version = String.Empty;

            //Get the binary paths for 17 and 18
            String version17Binaries = iniFile.getValue("17.00", "GPSPath");
            String version18Binaries = iniFile.getValue("18.00", "GPSPath");
            String mode = iniFile.getValue("GPS", "Mode");

            // Make sure at least one binary section is valid
            if (version17Binaries == String.Empty && version18Binaries == String.Empty)
            {
                MessageBox.Show("Unable to find binary directory in either section [18.00] or [17.00]!");
                return;
            }

            // If a mode is set, make sure the binary directory exists for that section
            if(mode == "18.00")
            {
                if (version18Binaries == String.Empty)
                    throw new Exception("GPS.INI specifies Mode 18 but has no binary path for 18.00!");
                
                version = mode;
                binaryPathToUse = version18Binaries;
            }
            else if(mode == "17.00")
            {
                if (version17Binaries == String.Empty)
                    throw new Exception("GPS.INI specifies Mode 17 but has no binary path for 17.00!");
                
                version = mode;
                binaryPathToUse = version17Binaries;
            }
            else // Otherwise, take the greater of the sections
            {
                if(version18Binaries != String.Empty)
                {
                    version = "18.00";
                    binaryPathToUse = version18Binaries;
                }
                else
                {
                    version = "17.00";
                    binaryPathToUse = version17Binaries;
                }
            }

            GpsElements elements = this.ExtractElementsFromGpsIni(binaryPathToUse, version, iniFile);

            this.PopulateFormFieldsFromElements(elements);
        }

        // Extracts properties from ini file and creates a GpsElements object
        private GpsElements ExtractElementsFromGpsIni(String binaryPath, String version, IniFile iniFile)
        {
            GpsElements elements = new GpsElements();

            elements.BinaryPath = binaryPath;
            elements.CurrentGPSVersion = version;
            elements.AllVersions = version;
            
            elements.CrystalReportsPath = iniFile.getValue("GPS", "CrystalReportsPath");
            elements.GPSSourceFilesPath = iniFile.getValue("GPS", "GPSSourceFilesPath");
            elements.ExcelPath = iniFile.getValue("GPS", "ExcelPath");
            elements.ReaderPath = iniFile.getValue("GPS", "ReaderPath");
            elements.GPSWorkFilesPath = SystemEnvironment.GpsLocalAppDataFolder;
            elements.GPSUserPreferencePath = SystemEnvironment.GpsLocalAppDataFolder;
            elements.ReaderSwitches = iniFile.getValue("GPS", "ReaderSwitches");
            elements.Gateway = iniFile.getValue("GPS", "Gateway");
            elements.ReaderOverride = iniFile.getValue("GPS", "ReaderOverride");
            elements.Mode = iniFile.getValue("GPS", "Mode");
            elements.FileTag1 = iniFile.getValue("ConfigFiles", "FileTag1");
            elements.FileTag2 = iniFile.getValue("ConfigFiles", "FileTag2");
            elements.FileTag3 = iniFile.getValue("ConfigFiles", "FileTag3");
            elements.TEMPLATE_TREE = iniFile.getValue("ConfigFiles", "TEMPLATE_TREE");
            elements.TEMPLATE_TREE_V18 = iniFile.getValue("ConfigFiles", "TEMPLATE_TREE_V18");
            elements.DD_CONFIG = iniFile.getValue("ConfigFiles", "DD_CONFIG");
            elements.NumberOfTags = iniFile.getValue("ConfigFiles", "NumberOfTags");
            elements.SerializeWaterMark = iniFile.getValue("Defaults", "SerializeWaterMark");
            elements.SaveReminderInterval = iniFile.getValue("Defaults", "SaveReminderInterval");
            elements.NumberOfArchivedNodes = iniFile.getValue("Defaults", "NumberOfArchivedNodes");
            elements.EnableSaveReminder = iniFile.getValue("Defaults", "EnableSaveReminder");
            elements.NbrOfComponents = iniFile.getValue("GPS Components", "NbrOfComponents");
            for (int i = 1; i < 100; i++)
            {
                String value = iniFile.getValue("GPS Components", "Component" + i);
                if (value == String.Empty)
                    break;

                elements.Components.Add(value);
            }

            if(elements.Gateway.Length > 0)
                elements.Schema = SchemaGatewayMap.LookupSchema(elements.Gateway);

            return elements;
        }

        // Populate the form fields using a GpsElement object
        private void PopulateFormFieldsFromElements(GpsElements elements)
        {
            inProcessOfUpdating = true;
            // General and GPS section
            this.txtBinaryDirectory.Text = elements.BinaryPath;
            this.txtVersion.Text = elements.CurrentGPSVersion;
            this.txtGpsUser.Text = elements.BinaryPath;

            String crystalReportPath = elements.CrystalReportsPath;
            String crystalReportDirectoryName = System.IO.Path.GetFileName(elements.CrystalReportsPath);

            String sourceFilesPath = elements.GPSSourceFilesPath;
            String sourceFilesDirectoryname = System.IO.Path.GetFileName(sourceFilesPath);

            this.txtExcelPath.Text = elements.ExcelPath;
            this.txtReaderPath.Text = elements.ReaderPath;

            this.txtCrystalReportsPath.Text = crystalReportPath;
            this.txtCrystalFolderReportName.Text = crystalReportDirectoryName;
            
            this.txtGpsSourceFilesPath.Text = sourceFilesPath;
            this.txtSourceFileFolderName.Text = sourceFilesDirectoryname;

            this.txtGpsWorkFilesPath.Text = elements.GPSWorkFilesPath;
            this.txtGpsUserPreferencePath.Text = elements.GPSUserPreferencePath;

            this.txtReaderSwitches.Text = elements.ReaderSwitches;
            int index = this.cbxGateway.FindString(elements.Gateway);
            this.cbxGateway.Text = elements.Gateway;

            this.ckbxReaderOverride.Checked = elements.ReaderOverride == "TRUE" ? true : false;
            this.txtMode.Text = elements.Mode;

            // ConfigFiles section
            this.txtFileTag1.Text = elements.FileTag1;
            this.txtFileTag2.Text = elements.FileTag2;
            this.txtFileTag3.Text = elements.FileTag3;
            this.txtTemplateTree.Text = elements.TEMPLATE_TREE;
            this.txtTemplateTreeV18.Text = elements.TEMPLATE_TREE_V18;
            this.txtDDConfig.Text = elements.DD_CONFIG;
            this.txtNumberOfTags.Text = elements.NumberOfTags;

            // Defaults
            this.txtSerializeWaterMark.Text = elements.SerializeWaterMark;
            this.txtSaveReminderInterval.Text = elements.SaveReminderInterval;
            this.txtNumberOfArchivedNodes.Text = elements.NumberOfArchivedNodes;
            this.ckbxEnableSaveReminder.Checked = elements.EnableSaveReminder == "1" ? true : false;

            // GPS Components
            this.lstComponents.Items.Clear();
            foreach(String value in elements.Components)
                this.lstComponents.Items.Add(value);
            this.txtNbrOfComponents.Text = elements.NbrOfComponents;

            // Form special
            this.txtGpsContIniPath.Text = String.Empty;
            if (!String.IsNullOrEmpty(elements.BinaryPath))
                this.txtGpsContIniPath.Text = elements.BinaryPath + "\\GPSCONT.INI";

            this.txtSchema.Text = elements.Schema;

            inProcessOfUpdating = false; 
        }

        // Saves a GPS.INI file to a specified path
        private void SaveIniFile(String path)
        {
            GpsElements elements = ExtractElementsFromForm();

            GPSIniWriter iniWriter = new GPSIniWriter(new IniFileWriter(path));
            iniWriter.WriteElements(elements);
        }

        // Extracts property values from the form and returns a GpsElement object
        private GpsElements ExtractElementsFromForm()
        {
            GpsElements elements = new GpsElements();

            elements.BinaryPath = this.txtBinaryDirectory.Text;
            elements.ExcelPath = this.txtExcelPath.Text;
            elements.ReaderPath = this.txtReaderPath.Text;
            elements.ReaderSwitches = this.txtReaderSwitches.Text;
            elements.ReaderOverride = this.ckbxReaderOverride.Checked ? "TRUE" : "FALSE";
            elements.Gateway = this.cbxGateway.Text;
            elements.GPSUser = this.txtGpsUser.Text;
            elements.CurrentGPSVersion = this.txtVersion.Text;
            elements.AllVersions = this.txtVersion.Text;
            elements.CrystalReportsPath = this.txtCrystalReportsPath.Text;
            elements.GPSSourceFilesPath = this.txtGpsSourceFilesPath.Text;
            elements.GPSWorkFilesPath = this.txtGpsWorkFilesPath.Text;
            elements.GPSUserPreferencePath = this.txtGpsUserPreferencePath.Text;
            elements.LastPCSetupDate = "02-Aug-10";
            elements.PasswordServer = "02-US_PSWSRV_01-10";
            elements.UserDir = "Y";
            elements.Mode = this.txtMode.Text;
            elements.FileTag1 = this.txtFileTag1.Text;
            elements.FileTag2 = this.txtFileTag2.Text;
            elements.FileTag3 = this.txtFileTag3.Text;
            //elements.NumberOfTags = this.txtNumberOfTags.Text;
            ushort fileTagCount = 0;
            if (elements.NumberOfTags.Trim() == String.Empty)
            {
                if (elements.FileTag1 != String.Empty)
                    ++fileTagCount;
                if (elements.FileTag2 != String.Empty)
                    ++fileTagCount;
                if (elements.FileTag3 != String.Empty)
                    ++fileTagCount;
                elements.NumberOfTags += fileTagCount;
            }
            elements.TEMPLATE_TREE = this.txtTemplateTree.Text;
            elements.TEMPLATE_TREE_V18 = this.txtTemplateTreeV18.Text;
            elements.DD_CONFIG = this.txtDDConfig.Text;
            elements.SerializeWaterMark = this.txtSerializeWaterMark.Text;
            elements.EnableSaveReminder = this.ckbxEnableSaveReminder.Checked ? "1" : "0";
            elements.SaveReminderInterval = this.txtSaveReminderInterval.Text;
            elements.NumberOfArchivedNodes = this.txtNumberOfArchivedNodes.Text;
            for (int i = 0; i < lstComponents.Items.Count; i++)
                elements.Components.Add(lstComponents.Items[i].ToString());
            elements.NbrOfComponents = lstComponents.Items.Count.ToString() ;
            elements.GPSPath = this.txtBinaryDirectory.Text;

            if (elements.Gateway.Length > 0)
                elements.Schema = SchemaGatewayMap.LookupSchema(elements.Gateway);

            return elements;
        }

        // Reads a specified path to the form preview box
        private void ReadFileToPreviewBox(String path)
        {
            String text = System.IO.File.ReadAllText(path);
            this.rtxIniText.Text = text;
        }

        // Saves the current form properties to a profile with the specified profile name.
        private void SaveProfile()
        {
            if (this.txtProfileName.Text == String.Empty)
                return;

            String gpsIniDataDirectory = SystemEnvironment.GpsIniAppDataFolder;
            String fullProfilePath = gpsIniDataDirectory + txtProfileName.Text + ".ini";

            this.SaveIniFile(fullProfilePath);
            this.RefreshProfileList();
        }

        // Updates the GPSCONT.INI file with schema change mapped by form's gateway
        private Boolean UpdateGpsContIniFile()
        {
            GpsElements elements = this.ExtractElementsFromForm();
            
            String gpsContPath = elements.BinaryPath + "\\" + "GPSCONT.INI";
            if(!System.IO.File.Exists(gpsContPath))
            {
                MessageBox.Show(
                    String.Format("GPS Ini Profile was unable to find a valid GPSCont.ini file in the specified location:\r\n{0}\r\n\r\n"
                    + "Check to make sure your have a valid GPSCont.ini file in your binary directory.",
                    gpsContPath), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error
                    );
                return false;
            }

            IniFile gpsContIniFile = new IniFile(gpsContPath);
            gpsContIniFile.replaceValue("GPS", "Gateway", elements.Schema);
            return true;
        }

        // Refreshes the profile combo box
        private void RefreshProfileList()
        {
            String[] files = System.IO.Directory.GetFiles(SystemEnvironment.GpsIniAppDataFolder, "*.ini");
            List<String> fileNameList = new List<String>();

            foreach(String file in files)
            {
                String fileName = System.IO.Path.GetFileName(file);
                fileNameList.Add(fileName.Remove(fileName.Length - 4));
            }

            this.cbxLoadProfile.Items.Clear();
            this.cbxLoadProfile.Items.AddRange(fileNameList.ToArray());
        }

        private void OnForm_Load(object sender, EventArgs e)
        {
            this.RefreshProfileList();
        }

        // Return the full path of a profile
        private String GetSavedProfilePath(String profileName)
        {
            return SystemEnvironment.GpsIniAppDataFolder + profileName + ".ini";
        }

        private Boolean SaveToIniFiles()
        {
            // Do blank checkinghere
            const String GPS_INI_LOCATION = "C:\\ProgramData\\GPS\\GPS.ini";

            SaveIniFile(GPS_INI_LOCATION);
            ReadFileToPreviewBox(GPS_INI_LOCATION);
            return UpdateGpsContIniFile();
        }

        // Update Events
        #region Update Events
        // When the template tree edit box has changed
        private void TemplateTree_Update(object sender, EventArgs e)
        {
            this.txtTemplateTree.Text = this.txtBinaryDirectory.Text;
            this.txtTemplateTree.Text += "\\";
            this.txtTemplateTree.Text += this.txtFileTag1.Text;

            GpsElements elements = this.ExtractElementsFromForm();
            this.txtNumberOfTags.Text = elements.NumberOfTags;
        }

        // When the DDConfig edit box has changed
        private void DDConfig_Update(object sender, EventArgs e)
        {
            this.txtDDConfig.Text = this.txtBinaryDirectory.Text;
            this.txtDDConfig.Text += "\\";
            this.txtDDConfig.Text += this.txtFileTag2.Text;

            GpsElements elements = this.ExtractElementsFromForm();
            this.txtNumberOfTags.Text = elements.NumberOfTags;
        }

        // When the template tree V18 edit box has changed
        private void TemplateTreeV18_Update(object sender, EventArgs e)
        {
            this.txtTemplateTreeV18.Text = this.txtBinaryDirectory.Text;
            this.txtTemplateTreeV18.Text += "\\";
            this.txtTemplateTreeV18.Text += this.txtFileTag3.Text;

            GpsElements elements = this.ExtractElementsFromForm();
            this.txtNumberOfTags.Text = elements.NumberOfTags;
        }

        // When the FileTag1 edit box has changed
        private void FileTag1_Update(object sender, EventArgs e)
        {
            TemplateTree_Update(sender, e);
        }

        // When the FileTag2 edit box has changed
        private void FileTag2_Update(object sender, EventArgs e)
        {
            DDConfig_Update(sender, e);
        }

        // When the FileTag3 edit box has changed
        private void FileTag3_Update(object sender, EventArgs e)
        {
            TemplateTreeV18_Update(sender, e);
        }

        // When the gateway combo box changes
        private void GatewayHasChanged_Update(object sender, EventArgs e)
        {
            String schema = SchemaGatewayMap.LookupSchema(this.cbxGateway.Text);

            if (schema.Length > 0)
                this.txtSchema.Text = schema;

            Field_Update(sender, e);
        }

        // When the binary edit box has changed
        private void BinaryDirectory_Update(object sender, EventArgs e)
        {
            this.txtGpsUser.Text = this.txtBinaryDirectory.Text;

            this.txtGpsContIniPath.Text = String.Empty;
            if (!String.IsNullOrEmpty(this.txtGpsUser.Text))
                this.txtGpsContIniPath.Text = this.txtGpsUser.Text + "\\GPSCONT.INI";

            SourceFileFolder_Update(sender, e);
            CrystalFolderReportName_Update(sender, e);
            TemplateTree_Update(sender, e);
            DDConfig_Update(sender, e);
            TemplateTreeV18_Update(sender, e);
        }

        // When the Crystal Folder Report Name edit box has changed
        private void CrystalFolderReportName_Update(object sender, EventArgs e)
        {
            this.txtCrystalReportsPath.Text = this.txtGpsUser.Text;
            this.txtCrystalReportsPath.Text += "\\";
            this.txtCrystalReportsPath.Text += this.txtCrystalFolderReportName.Text;
        }

        // When the Gps Source File Folder edit box has changed
        private void SourceFileFolder_Update(object sender, EventArgs e)
        {
            this.txtGpsSourceFilesPath.Text = this.txtGpsUser.Text;
            this.txtGpsSourceFilesPath.Text += "\\";
            this.txtGpsSourceFilesPath.Text += this.txtSourceFileFolderName.Text;

        }

        // Anytime a field is updated
        private void Field_Update(object sender, EventArgs e)
        {
            if (inProcessOfUpdating)
                return;

            GPSIniWriter iniWriter = new GPSIniWriter(new IniStringWriter());

            GpsElements elements = this.ExtractElementsFromForm();
            iniWriter.WriteElements(elements);

            this.rtxIniText.Text = iniWriter.ToString();

        }

        // When the saved profile combo box is changed
        private void Profile_Update(object sender, EventArgs e)
        {
            String profileName = this.cbxLoadProfile.SelectedItem.ToString();
            String profilePath = this.GetSavedProfilePath(profileName);

            this.LoadIniFile(profilePath);
            this.txtProfileName.Text = profileName;
        }
        #endregion

        // Button Click Events
        #region Button Click Events

        // Loading an Ini file
        private void LoadIniFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog fileDialog = new OpenFileDialog();
            fileDialog.Title = "GPS Configuration File";
            fileDialog.Filter = "Configuration|*.ini";
            fileDialog.InitialDirectory = SystemEnvironment.GpsProgramDataFolder;

            if (fileDialog.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                return;

            String fullFilePath;
            fullFilePath = System.IO.Path.GetDirectoryName(fileDialog.FileName);
            fullFilePath += "\\";
            fullFilePath += System.IO.Path.GetFileName(fileDialog.FileName);
            this.LoadIniFile(fullFilePath);
        }

        // Saving to GPS.INI and GPSCONT.ini and launching the application
        private void ApplyToIniFiles_Click(object sender, EventArgs e)
        {
            // Check to confirm form is valid
            GpsElements elements = this.ExtractElementsFromForm();
            String errorMessages = String.Empty;
            if (!elements.CheckRequiredFields(out errorMessages))
            {
                MessageBox.Show(errorMessages, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            // Save ini files
            if (!SaveToIniFiles())
                return;

            StringBuilder potentialError = new StringBuilder();
            // Execute GPS
            String launchPath = elements.BinaryPath + "\\" + SystemEnvironment.GpsLaunchName;

            // If it's not in the binary directory, it might be one level up such as installation
            if (!System.IO.File.Exists(launchPath))
            {
                potentialError.Append(launchPath).Append("\r\n");
                launchPath = elements.BinaryPath + "\\..\\" + SystemEnvironment.GpsLaunchName;
                potentialError.Append(launchPath).Append("\r\n");
            }

            if(!System.IO.File.Exists(launchPath))
            {
                MessageBox.Show(
                    String.Format("Unable to find the GPS to launch using path:\r\n{0}\r\n\r\n"
                    + "Make sure your binary path has the valid files for GPS.",
                    potentialError.ToString()), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error
                    );
                return;
            }

            // Perform launch
            Process gpsProcess = new Process();

            gpsProcess.StartInfo.FileName = launchPath;
            gpsProcess.Start();
        }

        // Saving the profile
        private void SaveProfile_Click(object sender, EventArgs e)
        {
            this.SaveProfile();
        }

        // Deletes the active profile.
        private void DeleteProfile_Click(object sender, EventArgs e)
        {
            String fullProfilePath = SystemEnvironment.GpsIniAppDataFolder + this.txtProfileName.Text + ".ini";
            System.IO.File.Delete(fullProfilePath);

            this.RefreshProfileList();
            if (this.cbxLoadProfile.Items.Count < 1)
            {
                this.cbxLoadProfile.Text = String.Empty;
                //return;
            }

            this.txtProfileName.Text = String.Empty;
            //this.cbxLoadProfile.SelectedIndex = 0;
            this.rtxIniText.Text = String.Empty;
            GpsElements elements = new GpsElements();
            this.PopulateFormFieldsFromElements(elements);
        }

        // Adds component to component list
        private void AddComponentToList_Click(object sender, EventArgs e)
        {
            if (txtAddComponent.Text.Trim() == String.Empty)
                return;

            lstComponents.Items.Add(txtAddComponent.Text);
            Field_Update(sender, e);

            GpsElements elements = this.ExtractElementsFromForm();
            this.txtNbrOfComponents.Text = elements.NbrOfComponents;
        }

        // Removes component from component list
        private void RemoveComponentFromList_Click(object sender, EventArgs e)
        {
            var item = this.lstComponents.SelectedItem;
            this.lstComponents.Items.Remove(item);

            Field_Update(sender, e);

            GpsElements elements = this.ExtractElementsFromForm();
            this.txtNbrOfComponents.Text = elements.NbrOfComponents;
        }

        // When searching for the GPS binary directory
        private void BrowseForBinaryDirectory_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderBrowser = new FolderBrowserDialog();
            folderBrowser.Description = "Select the binary directory for GPS:";
            folderBrowser.ShowNewFolderButton = true;
            folderBrowser.SelectedPath = this.txtBinaryDirectory.Text;
            folderBrowser.RootFolder = System.Environment.SpecialFolder.MyComputer;

            DialogResult result = folderBrowser.ShowDialog();
            if (result == DialogResult.OK)
                this.txtBinaryDirectory.Text = folderBrowser.SelectedPath;
        }
        #endregion
    }
}

