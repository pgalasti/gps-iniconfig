﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IniConfig.Structure
{
    /// <summary>
    /// GPS.INI file elements. 
    /// </summary>
    class GpsElements
    {
        // GPS Section
        public const String GPS_SECTION = "GPS";
        public String ExcelPath;
        public String ReaderPath;
        public String ReaderSwitches;
        public String ReaderOverride;
        public String Gateway;
        public String GPSUser;
        public String CurrentGPSVersion;
        public String AllVersions;
        public String CrystalReportsPath;
        public String GPSSourceFilesPath;
        public String GPSWorkFilesPath;
        public String GPSUserPreferencePath;
        public String LastPCSetupDate;
        public String PasswordServer;
        public String UserDir;
        public String Mode;

        // ConfigFiles Section
        public const String CONFIG_FILES_SECTION = "ConfigFiles";
        public String ConfigFiles;
        public String NumberOfTags;
        public String FileTag1;
        public String FileTag2;
        public String FileTag3;
        public String TEMPLATE_TREE;
        public String TEMPLATE_TREE_V18;
        public String DD_CONFIG;

        // Defaults Section
        public const String DEFAULTS_SECTION = "Defaults";
        public String Defaults;
        public String SerializeWaterMark;
        public String EnableSaveReminder;
        public String SaveReminderInterval;
        public String NumberOfArchivedNodes;

        // GPS Components Section
        public const String GPS_COMPONENTS_SECTION = "GPS Components";
        public String NbrOfComponents;
        public List<String> Components;

        // Binaries section
        public String BinaryPath; // Path is the section text
        public String GPSPath;

        // Other
        public String Schema;

        /// <summary>
        /// Creates a new GpsElements object.
        /// </summary>
        public GpsElements()
        {
            this.ExcelPath = String.Empty;
            this.ReaderPath = String.Empty;
            this.ReaderSwitches = String.Empty;
            this.ReaderOverride = String.Empty;
            this.Gateway = String.Empty;
            this.GPSUser = String.Empty;
            this.CurrentGPSVersion = String.Empty;
            this.AllVersions = String.Empty;
            this.CrystalReportsPath = String.Empty;
            this.GPSSourceFilesPath = String.Empty;
            this.GPSSourceFilesPath = String.Empty;
            this.GPSWorkFilesPath = String.Empty;
            this.GPSUserPreferencePath = String.Empty;
            this.LastPCSetupDate = String.Empty;
            this.PasswordServer = String.Empty;
            this.UserDir = String.Empty;
            this.Mode = String.Empty;
            this.ConfigFiles = String.Empty;
            this.NumberOfTags = String.Empty;
            this.ConfigFiles = String.Empty;
            this.FileTag1 = String.Empty;
            this.FileTag2 = String.Empty;
            this.FileTag3 = String.Empty;
            this.TEMPLATE_TREE = String.Empty;
            this.TEMPLATE_TREE_V18 = String.Empty;
            this.DD_CONFIG = String.Empty;
            this.Defaults = String.Empty;
            this.SerializeWaterMark = String.Empty;
            this.EnableSaveReminder = String.Empty;
            this.SaveReminderInterval = String.Empty;
            this.NumberOfArchivedNodes = String.Empty;
            this.NbrOfComponents = String.Empty;
            this.BinaryPath = String.Empty;
            this.GPSPath = String.Empty;
            Schema = String.Empty;
            this.Components = new List<String>();
        }

        public Boolean CheckRequiredFields(out String errorMessage)
        {
            Boolean success = true;
            errorMessage = String.Empty;

            StringBuilder errorBuilder = new StringBuilder();

            if (!System.IO.Directory.Exists(this.BinaryPath))
            {
                errorBuilder.Append("Binary Path " + this.BinaryPath + " does not exist.\r\n");
                success = false;
            }

            if (!System.IO.Directory.Exists(this.CrystalReportsPath))
            {
                errorBuilder.Append("CrystalReportsPath " + this.CrystalReportsPath + " does not exist.\r\n");
                success = false;
            }

            if (!System.IO.Directory.Exists(this.GPSSourceFilesPath))
            {
                errorBuilder.Append("GPSSourceFilesPath  " + this.GPSSourceFilesPath + " does not exist.\r\n");
                success = false;
            }

            if (!System.IO.Directory.Exists(this.GPSWorkFilesPath))
            {
                errorBuilder.Append("GPSWorkFilesPath  " + this.GPSWorkFilesPath + " does not exist.\r\n");
                success = false;
            }

            if (!System.IO.Directory.Exists(this.GPSUserPreferencePath))
            {
                errorBuilder.Append("GPSUserPreferencePath  " + this.GPSUserPreferencePath + " does not exist.\r\n");
                success = false;
            }

            if (!System.IO.File.Exists(this.TEMPLATE_TREE))
            {
                errorBuilder.Append("TEMPLATE_TREE   " + this.TEMPLATE_TREE + " does not exist.\r\n");
                success = false;
            }

            if (!System.IO.File.Exists(this.DD_CONFIG))
            {
                errorBuilder.Append("DD_CONFIG  " + this.DD_CONFIG + " does not exist.\r\n");
                success = false;
            }

            if (!System.IO.Directory.Exists(this.GPSPath))
            {
                errorBuilder.Append("GPSPath  " + this.GPSPath + " does not exist.\r\n");
                success = false;
            }

            if(String.IsNullOrEmpty(this.Gateway))
            {
                errorBuilder.Append("The Gateway property is empty\r\n");
                success = false;
            }

            if(String.IsNullOrEmpty(this.ExcelPath))
            {
                errorBuilder.Append("The ExcelPath property is empty\r\n");
                success = false;
            }

            if(String.IsNullOrEmpty(this.ReaderPath))
            {
                errorBuilder.Append("The ReaderPath property is empty\r\n");
                success = false;
            }

            if (String.IsNullOrEmpty(this.GPSUser))
            {
                errorBuilder.Append("The GPSUser property is empty\r\n");
                success = false;
            }

            if (String.IsNullOrEmpty(this.FileTag1))
            {
                errorBuilder.Append("The FileTag1 property is empty\r\n");
                success = false;
            }

            if (String.IsNullOrEmpty(this.FileTag2))
            {
                errorBuilder.Append("The FileTag2  property is empty\r\n");
                success = false;
            }

            if (String.IsNullOrEmpty(this.FileTag2))
            {
                errorBuilder.Append("The FileTag2  property is empty\r\n");
                success = false;
            }

            errorMessage = errorBuilder.ToString();
            
            return success;
        }
    }
}
