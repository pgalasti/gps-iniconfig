﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IniConfig.IO
{

    // TODO
    // I should probably put the hardcoded values in some configuration file 
    //so I don't have to recompile if I need to make a future change


    /// <summary>
    /// Contains environment variables for the IniConfig program.
    /// </summary>
    class SystemEnvironment
    {
        /// <summary>
        /// Returns the GPS local app folder.
        /// </summary>
        public static String GpsLocalAppDataFolder
        {
            get
            {
                String gpsAppDataDirectory = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
                gpsAppDataDirectory += "\\GPS\\";

                return gpsAppDataDirectory;
            }
        }

        /// <summary>
        /// Returns the application private GPSIniConfig folder.
        /// </summary>
        public static String GpsIniAppDataFolder
        {
            get
            {
                String gpsIniDirectory = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
                gpsIniDirectory += "\\GPSIniConfig\\";

                if(!System.IO.Directory.Exists(gpsIniDirectory))
                    System.IO.Directory.CreateDirectory(gpsIniDirectory);

                return gpsIniDirectory;
            }
        }

        /// <summary>
        /// Returns the application private GPSIniConfig\temp folder.
        /// </summary>
        public static String GpsIniAppDataTempFolder
        {
            get
            {
                return GpsLocalAppDataFolder + "temp\\";
            }
        }

        /// <summary>
        /// Returns GPS's program data folder
        /// </summary>
        public static String GpsProgramDataFolder
        {
            get
            {
                return @"C:\ProgramData\GPS";
            }
        }

        /// <summary>
        /// The launch program for GPS
        /// </summary>
        public static String GpsLaunchName
        {
            get
            {
                return "GPSVer.exe";
            }
        }
    }
}
