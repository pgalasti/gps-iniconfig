﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

namespace IniConfig.Structure
{
    /// <summary>
    /// A simple structure to obtain ini file key/value pair information from.
    /// </summary>
    public class IniFile
    {
        [DllImport("kernel32")]
        static extern int GetPrivateProfileString(string Section, string Key, string Default, StringBuilder RetVal, int Size, string FilePath);
        [DllImport("kernel32")]
        static extern long WritePrivateProfileString(string section, string key, string val, string filePath);

        private String filePath;

        /// <summary>
        /// The file path of the ini file.
        /// </summary>
        public String FilePath
        {
            get { return this.filePath; }
            set 
            { 
                if(!value.EndsWith(".ini", true, null))
                {
                    throw new Exception("FilePath is being set with a non .INI file!");
                }

                this.filePath = value; 
            }
        }

        /// <summary>
        /// Creates a new IniFile object without a path.
        /// </summary>
        public IniFile()
        {
            this.filePath = "";
        }

        /// <summary>
        /// Creates a new IniFile object with a specified file path.
        /// </summary>
        /// <param name="filePath">A file path to an ini file</param>
        public IniFile(String filePath)
        {
            this.filePath = filePath;
        }

        /// <summary>
        /// Returns a value from a key/value pair under a particular section.
        /// </summary>
        /// <param name="section">The section a key is located in the ini file.</param>
        /// <param name="key">The key a value is associated with in the ini file</param>
        /// <returns></returns>
        public String getValue(String section, String key)
        {
            if(this.filePath.Length == 0)
                throw new Exception("IniFile::FilePath is blank!");

            if(!System.IO.File.Exists(this.filePath))
                throw new Exception("IniFile::FilePath path doesn't exist!");

            StringBuilder returnedValue = new StringBuilder(512);
            GetPrivateProfileString(section, key, "", returnedValue, 512, this.filePath);

            return returnedValue.ToString();
        }

        /// <summary>
        /// Replaces a section's key/value pair with the newValue parameter for the loaded ini file.
        /// </summary>
        /// <param name="section">Section the key/value pair is to be replaced</param>
        /// <param name="key">The key in the key/value pair</param>
        /// <param name="newValue">The new value in the key/value pair</param>
        public void replaceValue(String section, String key, String newValue)
        {
            long retCode = WritePrivateProfileString(section, key, newValue, this.filePath);

            if(retCode == WRITE_FAILURE)
                throw new System.IO.IOException("Unable to replace key/string value!");
        }

        const long WRITE_FAILURE = 0;
    }
}
