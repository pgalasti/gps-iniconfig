﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IniConfig.IO
{
    interface IIniWriter
    {
        void WriteSectionItem(String section);
        void WriteLineItem(String key, String value);
        void WriteComment(String comment);
        void WriteBlankLine();
        void OpenWriter(String path);
        void CloseWriter();
        String ToString();
    }
}
