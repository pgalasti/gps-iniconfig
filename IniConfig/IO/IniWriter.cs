﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IniConfig.IO
{
    /// <summary>
    /// Inherits from the IIniWriter interface.
    /// Writes ini string key/value pairs to specified ini file.
    /// </summary>
    class IniFileWriter : IIniWriter
    {
        protected StringBuilder lineBuilder;
        protected System.IO.StreamWriter streamWriter;
        protected String path;

        /// <summary>
        /// Creates a new IniFileWriter object.
        /// </summary>
        /// <param name="path">The path to the ini file being written to.</param>
        public IniFileWriter(String path)
        {
            this.lineBuilder = new StringBuilder();
            this.OpenWriter(path);
        }

        /// <summary>
        /// Writes a new INI section.
        /// </summary>
        /// <param name="section">The name of the new section</param>
        public void WriteSectionItem(String section)
        {
            if (this.streamWriter == null)
                throw new System.IO.IOException("Stream writer is not open to write line to ini file!");

            this.lineBuilder.Clear();

            this.lineBuilder.Append("[").Append(section).Append("]");
            this.streamWriter.WriteLine(lineBuilder.ToString());
        }

        /// <summary>
        /// Writes a new key/value pair item.
        /// </summary>
        /// <param name="key">The key value of the pair</param>
        /// <param name="value">The value value of the pair</param>
        public void WriteLineItem(String key, String value)
        {
            if (this.streamWriter == null)
                throw new System.IO.IOException("Stream writer is not open to write line to ini file!");

            this.lineBuilder.Clear();

            this.lineBuilder.Append(key).Append(" = ").Append(value);
            this.streamWriter.WriteLine(this.lineBuilder.ToString());
        }

        /// <summary>
        /// Writes a comment prefixed with a semi-colon.
        /// </summary>
        /// <param name="comment">An ini file comment</param>
        public void WriteComment(String comment)
        {
            this.lineBuilder.Clear();
            
            this.lineBuilder.Append(";").Append(comment);
            this.streamWriter.WriteLine(lineBuilder.ToString());
        }

        /// <summary>
        /// Writes a blank line to the Ini file.
        /// </summary>
        public void WriteBlankLine()
        {
            this.streamWriter.WriteLine();
        }

        /// <summary>
        /// Opens the writer to a file path.
        /// </summary>
        /// <param name="path">The file path the ini file will be written to</param>
        public void OpenWriter(String path)
        {
            this.path = path;
            this.streamWriter = new System.IO.StreamWriter(path);
        }

        /// <summary>
        /// Closees the writer from writing.
        /// </summary>
        public void CloseWriter()
        {
            this.streamWriter.Close();
            this.streamWriter = null;
        }

        /// <summary>
        /// Returns the ini file path
        /// </summary>
        /// <returns></returns>
        public override String ToString()
        {
            return this.path;
        }
    }
}
