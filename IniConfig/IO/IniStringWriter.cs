﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IniConfig.IO
{
    /// <summary>
    /// A simple class inheriting from IIniWriter to build the string value of an ini configuration.
    /// </summary>
    class IniStringWriter : IIniWriter
    {
        protected StringBuilder lineBuilder;
        protected StringBuilder stringBuilder;

        private const String NEW_LINE = "\r\n";

        /// <summary>
        /// Creates a new IniStringWriter object.
        /// </summary>
        public IniStringWriter()
        {
            this.lineBuilder = new StringBuilder(128);
            this.stringBuilder = new StringBuilder(2048);
        }

        /// <summary>
        /// The ini configuration string value.
        /// </summary>
        public String IniString
        {
            get
            {
                return this.stringBuilder.ToString();
            }
        }

        /// <summary>
        /// Writes a ini configuration section value to the configuration.
        /// </summary>
        /// <param name="section">The section name being written to the ini configuration</param>
        public void WriteSectionItem(string section)
        {
            this.lineBuilder.Clear();
            
            this.lineBuilder.Append("[").Append(section).Append("]");
            this.stringBuilder.Append(lineBuilder.ToString()).Append(NEW_LINE);
        }

        /// <summary>
        /// Writes a key/value item pair to the ini configuration.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void WriteLineItem(string key, string value)
        {
            this.lineBuilder.Clear();

            this.lineBuilder.Append(key).Append(" = ").Append(value);
            this.stringBuilder.Append(lineBuilder.ToString()).Append(NEW_LINE);
        }

        /// <summary>
        /// Writes a comment with a prefixed semicolon to the ini configuration.
        /// </summary>
        /// <param name="comment">The comment to be written to the ini configuration</param>
        public void WriteComment(string comment)
        {
            this.lineBuilder.Clear();

            this.lineBuilder.Append(";").Append(comment);
            this.stringBuilder.Append(lineBuilder.ToString()).Append(NEW_LINE);
        }

        /// <summary>
        /// Writes a blank line to the ini configuration.
        /// </summary>
        public void WriteBlankLine()
        {
            this.stringBuilder.Append(NEW_LINE);
        }

        /// <summary>
        /// Clears the ini configuration.
        /// </summary>
        public void ClearWriter()
        {
            this.stringBuilder.Clear();
        }

        /// <summary>
        /// Unsupported for IniStringWriter implementation.
        /// </summary>
        /// <param name="path"></param>
        public void OpenWriter(String path)
        {
            // Doesn't do anything
        }

        /// <summary>
        /// Unsupported for IniStringWriter implementation.
        /// </summary>
        public void CloseWriter()
        {
            // Doesn't do anything
        }

        /// <summary>
        /// Returns the full Ini configuration string.
        /// </summary>
        /// <returns>Ini configuration string</returns>
        public override String ToString()
        {
            return IniString;
        }
    }
}
